#Note: This first one can be skipped if the year remains 2020 as a dataset is provided within the repo
# git+https://gitlab.com/zdin-zle/models/dwd-data-downloader.git@main#egg=dwd-data-downloader 
git+https://gitlab.com/zdin-zle/models/photovoltaic.git@master#egg=mosaik-pv
arrow
pandas==1.4
mosaik-csv
mosaik-pandapower # Note: Requires python < 3.11
mosaik>=3.0
mosaik-hdf5
pandapower
matplotlib
openpyxl
seaborn
xlsxwriter
plotly
nbformat