'''
@author: Fernando Penaherrera @UOL/OFFIS

This file contains a helper function to rewrite CSV files to agree
with the MOSAIK CSV reader format
'''


def write_csv_files(filename, header):
    """Writes a csv of the column of a dataframe
    The output files need to be in the MOSAIK-CSV format::

        Braunschweig
        Date,SolarRad,Pressure,WindSpeed
        01.01.2014 00:00,0,1003,6.1
        01.01.2014 01:00,0,1050,5.8
        ...

    Args:
        filename (str): Filename of the CSV file
        header (str): New headear for the file

    Returns:
        None: Dummy return
    """
   
    # First prevent rewriting 
    with open(filename) as f:
        first_line = f.readline()
        if header in first_line[0:len(header)]:
            return None

    
    with open(filename, 'r+') as f:
        content = f.read()
        f.seek(0, 0)
        f.write(header + '\n' + content)
        return None

if __name__ == '__main__':
    pass