"""
@author: Fernando Penaherrera @UOL/OFFIS

"""
import itertools
import mosaik_api
from src.components.grid_observer import grid_observer


# SIMULATION META DATA
META = {
    "type": "time-based",
    "models": {
        "GridObserver": {
            "public": True,
            "params": ["model_params"],
            "attrs": [
                "status",  # The status of the grid
                "ext_grid_p_mw",  # This is a "Control" value that checks that this object runs
                # after the net simulation is finished
                "net",  # the whole MOSAIK pandapower net object
                "bus_violation",
            ],
        },
    },
    "extra_methods": [
        "add_controlled_buses",  # Adds the busses to be controlled
        "add_controlled_grid",  # Adds the MOSAIK grid object
        "get_entities",  # Retuurns a list with entities
    ],
}

# ------------INPUT-SIGNALS--------------------
# ext_grid_p_mw     # This is a "Control" value that checks that this object runs
# after the net simulation is finished
# net               # The whole MOSAIK pandapower net object
# bus_violation     # List with bus violations

# ------------OUTPUT-SIGNALS--------------------
# status           Status of the Grid. True = No violations, False = Violations [W]
# status',           # The status of the grid


class Sim(mosaik_api.Simulator):
    def __init__(self):
        """Initializer"""
        super(Sim, self).__init__(META)

        # assign properties
        self.step_size = None

        # create dict for entities and eid_counters
        self.entities = {}
        self.eid_counters = {}

        # init Simulator of model
        self.simulator = grid_observer.Simulator()

    def add_controlled_buses(self, buses):
        """Adds a list of controlled buses

        Args:
            buses (list): List of buses given in mosaik lists format.
        """

        self.simulator.add_controlled_buses(buses)

    def add_controlled_grid(self, grid):
        """Adds a MOSAIK grid to be monitored

        Args:
            grid (mosaik pandapower grid): A MOSAIK pandapower grid class to be monitored.
        """
        self.simulator.add_controlled_grid(grid)

    # init API
    def init(self, sid, time_resolution=1, start_date=None, step_size=1):
        """Initiate the simulator

        Args:
            sid (string): Calls string ID
            time_resolution (int, optional): Time resolution for simulation. Defaults to 1.
            start_date (int, optional): Stard date for simulation. Defaults to None.
            step_size (int, optional): Step size. Defaults to 1.

        Returns:
            dict: Metadata in the MOSAIK format
        """
        # assign properties
        self.sid = sid
        self.time_resolution = time_resolution
        self.step_size = step_size

        # return meta data
        return self.meta

    # create model
    def create(self, num, model_type):
        """Create a number of instances for the model

        Args:
            num (int): Number of entities. Can only be 1
            model_type (class): Description of the model type  class

        Raises:
            ValueError: Only one entity can be created

        Returns:
            list: A list with the entities properties as dictionary
        """

        if num != 1:
            raise ValueError("Only one entity can be created")

        counter = self.eid_counters.setdefault(model_type, itertools.count())

        # create list for entities
        entities = []

        # TODO refactor the string formating

        for _ in range(num):
            eid = "{}_{}".format(model_type, next(counter))  # Entities IDs

            # create new Grid Observer model
            model = self.simulator.add_model()

            # create full id
            full_id = self.sid + "." + eid

            self.entities[eid] = {
                "ename": eid,
                "etype": model_type,
                "model": model,
                "full_id": full_id,
            }
            entities.append({"eid": eid, "type": model_type, "rel": []})

        return entities

    def step(self, time, inputs, max_advance=360000):
        """Perform calculation step

        Args:
            time (int): Time step size
            inputs (dict): Dictionary with inputs for simulation. Must contain "ext_grid_p_mw".
            max_advance (int, optional): Max advance. Defaults to 360000.

        Returns:
            int: _description_
        """
        ext_grid_p_mw = None
        # Perform simulation step
        for eid, attrs in inputs.items():

            for attr, vals in attrs.items():
                ext_grid_p_mw = None

                if attr == "ext_grid_p_mw":
                    ext_grid_p_mw = list(vals.values())[0]

        # This value is only to concatenate the simulations in proper order.
        if ext_grid_p_mw:
            self.simulator.step(time)

        # Next timestamp for simulation
        return time + self.step_size

    # get data
    def get_data(self, outputs):
        """Get data for further symulations

        Args:
            outputs (dict): Dictionary with outputs in MOSAIK type

        Raises:
            ValueError: Unknown output attribute. Attribute must be in the simulator metadata

        Returns:
            dict: A dictionary with the output data in MOSAIK format
        """
        # create output data
        data = {}

        # loop over all output values
        for ename, attrs in outputs.items():

            # set data entry for model
            data[ename] = {}

            # loop over all attributes
            for attr in attrs:
                if attr not in self.meta["models"]["GridObserver"]["attrs"]:
                    raise ValueError("Unknown output attribute: %s" % attr)
                # Get model data

                if attr == "net":
                    data[ename][attr] = self.entities[ename]["model"].net
                else:
                    data[ename][attr] = getattr(self.entities[ename]["model"], attr)
        return data

    def get_entities(self):
        """Returns a list with entities

        Returns:
            list: A list with the entities and their properties as dictionary
        """
        # return entities of API
        return self.entities
