"""
@author: Fernando Penaherrera @UOL/OFFIS

This class represents a "divider" of the information  and an Agent to control the storage.

This represents a second control for the storage, which shall provide the extra power
if required by the grid  corrector

Control --> Battery (and others) --> Net --> Grid Corrector --> Node Agent --> Battery

"""


class GridNode(object):
    """
    This is an Agent which gathers information on node required
    Power Injection and corresponding bus number
    """

    p_inj = None  # The required power injection at the bus
    vm_pu = None  # The voltage level per unit at the bus
    storage = None  # Storage entitty to be controlled by this node actuator

    results = ["p_inj", "vm_pu"]  # List of results for the cosimulation

    def __init__(self, init_vals):
        """Initialization class

        Args:
            init_vals (dict): : Dictionary with the initial values.
                          It must contain the parameter "name" so it can be
                          properly matched with a bus::
                                init_vals= {"name":5}

        Raises:
            ValueError: init_vals must be giveb
            ValueError: init_vals keys can only be "name"
        """

        # assign init values
        if init_vals is None:
            raise ValueError(
                "'init_vals' for the Node Class must be of a single"
                + " dict with the format {'name':5}"
            )

        if init_vals is not None:
            for key, value in init_vals.items():
                if key != "name":
                    raise ValueError("Key can only be 'name'")
                setattr(self, key, value)

    def step(self, step_size, dict_input):
        """Advance the current model time

        Args:
            step_size (int): The step_size of the mosaik simulation
            dict_input (dict): Input dictionary with the results of the grid analysis.A dict with the format::

                dict_input = {
                            0: {"p_inj": 1234, "vm_pu": 0.985},
                            1: {"p_inj": -456, "vm_pu": 0.954},
                            ...
                            }
        """

        # The attribute name should be created by the Simulator Object using the number
        # of element created, and is used to find the corresponding bus
        bus_id = self.name
        sto = self.storage["model"]

        # This value is the extra "load"
        # negative if extra "discharging" is required
        p_inj = dict_input[bus_id]["p_inj"]

        if abs(p_inj) > 1:  # error of 1 W may be due to numeric calculations
            # Temporarly save the self_discharge value
            self_discharge = sto.self_discharge

            # the battery already "self discharged" so this is temporarely set to 0
            sto.self_discharge = 0

            # then perform an extra fictional step with the power
            sto.P_SET = p_inj

            # TODO The step value shall be dynamic.
            sto.step(15 * 60)  # Pass the step value later

            # Reset the self_discharge value since it needs to keep working
            sto.self_discharge = self_discharge

        self.p_inj = dict_input[bus_id]["p_inj"]  # pick up the power injection from the bus
        self.vm_pu = dict_input[bus_id]["vm_pu"]  # pick the voltage for later plotting


class Simulator(object):
    """Simulator class for the GridNode()
    Creates multiple instances and saves the results in an accesible list

    Args:
        object (class): Generic Class

    """

    def __init__(self):
        """Constructor"""
        # init data elements
        self.models = []  # List with all the models
        self.results = []  # List with the results

    def add_controlled_storages(self, storages):
        """Adds a list of storages to monitor

        Args:
            storages (list): List of storages given in mosaik lists format.
        """
        self.storages = [storages[k] for k in storages.keys()]
        for i in range(len(self.models)):
            self.models[i].storage = self.storages[i]

    def add_model(self, init_vals=None):
        """Create model instances

        Args:
            init_vals (dict, optional): Dictionary with the initial values. Defaults to None.
                          It must contain the parameter "name" so it can be
                          properly matched with a bus::
                                init_vals= {"name":5}.

        Returns:
            GridNode: GridNode object
        """
        # create grid_observer model
        model = GridNode(init_vals)

        # add model to model grid_observer
        self.models.append(model)

        # add list for simulation data
        self.results.append([])

        # return model
        return model

    def step(self, time, dict_input):
        """Performs a simulation step

        Args:
            time (int): Simulation step time.
            dict_input (dict): Input dictionary with the results of the grid analysis.
                           A dict with the format::
            dict_input = {
                          0: 1234,
                          1: 5000,
                          2: -2000,
                          ...
                        }
        """
        # Enumeration over all models in simulator
        for i, model in enumerate(self.models):
            # perform simulation step
            model.step(time, dict_input)

            # collect data of model and storage local
            for _, signal in enumerate(model.results):
                self.results[i].append(getattr(model, signal))
