"""
Mosaik interface for the control simulator.

@author: Christian Reinhold and Henrik Wagner elenia@TUBS

"""


import mosaik_api

from src.components.control import control


# SIMULATION META DATA
META = {
    "type": "time-based",
    "models": {
        "BEM": {
            "public": True,
            "params": ["init_vals"],
            "attrs": [
                "P_SET",
                "P_LOAD",
                "Q_LOAD",
                "P_PV",
                "P_MAX",
                "P_MIN",
                "P_SALDO",
                "P_SALDO_RES",
                "PROGNOSIS_START",
                "PROGNOSIS_END",
                "RES_SIGNAL",
                "SIGNAL_PROGNOSIS_CS_control",
                "SIGNAL_PROGNOSIS",
                "PROGNOSIS_LOAD",
                "PROGNOSIS_PV",
                "PROGNOSIS_RESIDUAL",
            ],
        },
        "AEM": {"public": True, "params": [], "attrs": []},
    },
    "extra_methods": ["add_controlled_entity", "get_entities"],
}

# ------------INPUT-SIGNALS--------------------
# P_PV                  Actual Active Power PV [W]
# P_LOAD                Active Power Load [W]
# Q_LOAD                Reactive Power Load [var]
# P_MAX                 Maximal active power [W]
# P_MIN                 Minimal active power  [W]

# ------------OUTPUT-SIGNALS--------------------
# P_SET                 Set-Point for components [W]


class Sim(mosaik_api.Simulator):
    # contructor
    def __init__(self):
        super(Sim, self).__init__(META)
        self.step_size = None
        self.entities = {}

        # assign simulator
        self.simulator = control.Simulator()

    # init API
    def init(self, sid, time_resolution, step_size=1):
        # assign properties
        self.sid = sid
        self.step_size = step_size

        # return meta data
        return self.meta

    # create model
    def create(self, num, model_type, init_vals=None):
        # get next id
        next_eid = len(self.entities)
        # create list for entities
        entities = []

        # loop over all entities
        for i in range(next_eid, next_eid + num):

            # create name of model
            ename = "%s%s%d" % (model_type, "_", i)

            # create new model with init values
            model = self.simulator.add_model(
                model_type, ename, init_vals[i] if init_vals is not None else None
            )

            # add model information
            self.entities[ename] = {"ename": ename, "etype": model_type, "model": model}

            # append entity to list
            entities.append({"eid": ename, "type": model_type})

        # return created entities
        return entities

    # perfom calculation step
    def step(self, time, inputs, max_advance):

        # loop over all models
        for eid, attrs in inputs.items():
            # loop over all attributes
            for attr, values in attrs.items():
                # assign properties with sid
                setattr(self.entities[eid]["model"], attr, attrs.get(attr, {}))

        # perform simulation step
        self.simulator.step(time)

        # set data element
        data = self.simulator.get_set_data(inputs)

        # send data to other simulators/models
        yield self.mosaik.set_data(data)

        # return actual time for mosaik
        return time + self.step_size

    # get data
    def get_data(self, outputs):
        # create output data
        data = {}

        # loop over all output values
        for ename, attrs in outputs.items():
            # get model entry
            entry = self.entities[ename]

            # set data entry for model
            data[ename] = {}

            # loop over all attributes
            for attr in attrs:
                if attr not in self.meta["models"][entry["model"].type]["attrs"]:
                    raise ValueError("Unknown output attribute: %s" % attr)

                # Get model data
                data[ename][attr] = getattr(entry["model"], attr)

        # return data to mosaik
        return data

    # add component to control unit
    def add_controlled_entity(self, control_units, models):

        # add controlled component to BEM
        self.simulator.add_controlled_entity(control_units, models)

    # get entities
    def get_entities(self):
        # return entities of API
        return self.entities


def main():
    return mosaik_api.start_simulation(Sim())
