"""
@author: Fernando Penaherrera @UOL/OFFIS and Henrik Wagner @elenia/TUBS

Common parameters to be used through the src and subfolders.
Contains PATHS, SCENARIOS and CHARGING STRATEGIES definitions.

"""
from enum import Enum
import os
from os.path import join


def get_project_root():
    """Returns the path to the project root directory.

    Returns:
        str: A string with the project root directory
    """
    return os.path.realpath(
        os.path.join(
            os.path.dirname(__file__),
            os.pardir,
        )
    )


BASE_DIR = get_project_root()
DATA_DIR = join(BASE_DIR, "data")
DATA_PROCESSED_DIR = join(DATA_DIR, "processed")
DATA_RAW_DIR = join(DATA_DIR, "raw")
DATA_CAR_DIR = join(DATA_RAW_DIR, "car")
DATA_PV_DIR = join(DATA_PROCESSED_DIR, "pv")
DATA_LOAD_DIR = join(DATA_PROCESSED_DIR, "load")
RESULTS_DIR = join(BASE_DIR, "results")
RESULTS_DB_DIR = join(RESULTS_DIR, "databases")
GRID_SNAPS_DIR = join(RESULTS_DIR, "grid_snapshots")
RESULTS_POSTPROC_DIR = join(RESULTS_DIR, "postprocessed")
SRC_DIR = join(BASE_DIR, "src")
SCENARIOS_DIR = join(SRC_DIR, "scenarios")
PREPROC_DIR = join(SRC_DIR, "data_processing")
POSTPROC_DIR = join(SRC_DIR, "postprocessing")
TEST_DIR = join(BASE_DIR, "tests")


class Scenarios(Enum):
    """
    List of the available SIMULATION SCENARIOS

    TEST: Represents the base co-simulation containing only the electric grid model and household loads.

    EV: The present grid capacity is analyzed in the scenario EV. All following scenarios
    inclue adding user-sided measures to enhance the grid's capacity for EV.

    EV_PV: The scenario EV_PV extends the status quo by adding photovoltaic systems.

    EV_PV_STO: The scenario EV_PV_STO further expands previous scenarios by adding battery storages which provide
    flexibility to store the surplus of produced PV energy that can be used later on EV charging. Therefore dezentralized
    control units on building level are added.

    GRID_OBSV: The scenario GRID_OBSV considers all of the previous components and adds a centralized Grid Observer
    to avoid violations of the voltage range by power injections from the battery storages.
    As flexibility for the power injections, 10% of the nominal values of the corresponding
    storages charge/discharge power and capacity is reserved.

    """

    TEST = 0
    EV = 1
    EV_PV = 2
    EV_PV_STO = 3
    GRID_OBSV = 4


class Charging_Strategies(Enum):
    """max_P: The maximum power charging strategy charges the EV with the rated power (default 11 kW)
    of the charging station as soon as it arrives at the home location.

    forecast: The forecast-based charging strategy
    estimates the total standing time of the connected EV at
    home and then calculates the minimal charging power needed
    to fully charge the EV. As this spreads the charging process,
    the strategy dramatically reduces the needed level of charging
    power following a lower grid electricity demand per timestep.
    In this study, an ideal forecast of the mobility behavior is
    assumed and therefore the charging process is never interrupted
    nor corrected.

    solar_charging: The solar charging strategy is initiated within times of solar energy surplus.
    As soon as the energy needed for the next EV trip cannot be charged
    by solar energy surplus, the strategy temporarily automatically changes to to forecast-based
    charging, increasing the charging power to a sufficient level.

    night_charging: The night charging strategy extends the charging process
    similar to forecast-based charging. The strategy is solely activated
    and processed overnight between 20:00 and 05:00 when
    grid load is mostly low or lower in residential areas. Therefore,
    the times with low grid load are matched with power-intensive
    charging processes. When the EV is not available overnight,
    maximum power charging is automatically activated.

    Args:
        Enum (class): Defines new enumeration class for charging strategies.
    """

    max_P = 0
    forecast = 1
    night_charging = 2
    solar_charging = 3
    random = 4


if __name__ == "__main__":
    print(DATA_DIR)
