# GridEVA - Analysis of the Grid Capacity for Electric Mobility in Existing Districts with a Major Need for Energy Refurbishment

The developed tool GridEVA (Grid Electric Vehicle Analysis) aims to analyze the effects and impacts of an increasing EV penetration rate on the low-voltage grid in districts and identify the maximum possible grid capacity for EV charging using the co-simulation framework mosaik 3.x. A concrete use case is the existing residential district "Am Ölper Berge" in Brunswick, Lower Saxony, Germany. A different use case can be applied by exchanging the input grid data. 

## Table of Contents
- [Installation instructions](#installinginstr)
- [Co-Simulation Structure and models](#structure)
- [Key References](#keyreferences)
- [Contributing](#contribution)
- [Citation](#citations)
- [Authors & Acknowledgments](#authorsackn)
- [Read-the-docs](#read-the-docs)
- [License](#license)
  
## <a name="installinginstr"></a> Installation instructions
1. Create a virtual environment, for example:

    `conda create -n <env> python=3.8`

2. Activate the environment

    `conda activate <env>`

3. Download or clone this repository

4. Change the folder to the downloaded repository

    `cd <path_to_folder>`

5. From the cloned path, install the requirements

    `pip install -r requirements.txt`

6. Run the simulation with the following command:

    `python main.py`

7. The postprocessing of data is done interactively in the Jupyter Notefooks found under

    `evaluation\..`

8. To create a local copy of the repository, use the follwing command:

    `git clone https://gitlab.com/zdin-zle/scenarios/grid-capacity-for-electric-mobility.git`

9. To avoid conflicts with paths or internal imports, run the following command:

    `pip install -e .`
<!--## <a name="usage"></a> Usage
.......-->

<!--## <a name="overview"></a> Overview of the used simulators and simulation models
![](docs/Overview_scenario.png)
**Figure 1:** Overview of the simulators and simulation models

As shown in the figure, a model for mapping the user-dependent grid load caused by electric mobility is coupled with the grid calculation tool [pandapower](https://www.pandapower.org/) through the co-simulation platform mosaik. The electric mobility model provides time-dependent power values of the charging processes occurring in the district. The user-specific behaviour (charging frequency, charging time), current penetration rates and common charging capacities, coincidence factors and the region-specific typical path lengths are considered. Pandapower can be used to determine the loads on the local low-voltage grid in the neighbourhood. For this purpose, a load flow calculation on an annual basis, e.g. using type days, is planned. The required household and charging load profiles are imported using mosaik's CSV reader. The final visualisation will illustrate relevant technical key figures. In addition, the impact of electric mobility in the district on the electricity grid will be presented in a suitable format for interested stakeholders.-->

## <a name="structure"></a> Co-Simulation structure and models
The developed co-simulation for the energy system analysis follows mosaik’s object-oriented and modular structure. MOSAIK enables orchestration and exchange of parameters between the components and synchronizes the processes of the components and schedules their step-wise execution.

<img src="docs/cosim_structure_EIS22.png"  width="520" height="350">

<!--![](docs/cosim_structure_EIS22.png)-->
**Figure 1:** Overview of co-simulation structure, models and data flow for the analysis of the grid capacity for electric vehicles.

## <a name="keyreferences"></a> Key References

The contents in this repository have been developed based on **three key references**:

### Co-Simulation-Based Analysis of the Grid Capacity for Electric Vehicles in Districts: The Case of "Am Ölper Berge" in Lower Saxony

[Link to publication](https://doi.org/10.1049/icp.2022.2713) || [Link to exemplary simulation results](https://doi.org/10.5281/zenodo.14040583)

<details>
<summary>Citation</summary>
H. Wagner, F.Peñaherrera, S. Fayed, O. Werth, S. Eckhoff, B. Engel, M. H. Breitner, S. Lehnhoff, J. Rolink, ‘Co-Simulation-Based Analysis of the Grid Capacity for Electric Vehicles in Districts: The Case of “Am Ölper Berge” in Lower Saxony’, in 6th E-Mobility Power System Integration Symposium, The Hague, Netherlands, Oct. 2022. DOI: https://doi.org/10.1049/icp.2022.2713. 
</details>

### Overview
A co-simulation to determine the grid capacity for EV in districts has been presented and applied to the residential district “Am Ölper Berge” in Brunswick, Lower Saxony in Germany. The simulation period is set to analyze the worst-case and hence, the highest possible electricity demand resulting from the EVs. Within multiple scenarios, self-developed usersided measures for cooperative energy generation, storage, and smart charging strategies have been applied to enhance the grid’s capacity for EVs by improvements in voltage regulation.

### Relevance to this repository
This repository belongs to the results of the applied study. Within multiple scenarios, usersided measures for cooperative energy generation, storage, and smart charging strategies have been applied to enhance the grid’s capacity for EVs by improvements in voltage regulation. 

#### Simulation Scenarios 

- Scenario.EV: status quo - grid capacity of 245 EVs

- Scenario.EV_PV - solely applying PV systems with uncontrolled charging does not increase grid capacity 

- Scenario.EV_PV_STO -  only slight increase, battery storage sizing has only small effect with uncontrolled charging
- Scenario.GRID_OBSV -  applying the grid corrector for an active voltage regulation together with PV systems and battery storages (10% reserved flexibility) is the most effective combination of user-sided measures – using the reserved flexibility for power injections

<img src="docs/EIS22_gridcapacity_all.png"  width="540" height="250">

**Figure 2:** Grid Capacity for EVs in “Am Ölper Berge” – all scenarios

#### Charging strategies
- Maximum Power Charging
- Solar Charging
- Forecast-Based Charging
- Night Charging


<img src="docs/EIS22_min_voltage.png"  width="530" height="420">

**Figure 3:** Minimum voltage levels – controlled EV charging

## An Open Source Grid Observer for the Analysis of Power Flexibilities in Low Voltage Distribution Grid Simulations
[Link to publication](https://ieeexplore.ieee.org/document/9953716)
<details>
<summary>Citation</summary>
S. Fayed, F. Peñaherrera V., H. Wagner and J. Rolink, "An Open Source Grid Observer for the Analysis of Power Flexibilities in Low Voltage Distribution Grid Simulations," 2022 10th International Conference on Smart Grid and Clean Energy Technologies (ICSGCE), Kuala Lumpur, Malaysia, 2022, pp. 1-8, doi: 10.1109/ICSGCE55997.2022.995371      
</details>

### Overview
A tool For the detection and correction of voltage range violations at individual buses of a grid was developed and tested in a quasi-dynamic energy system simulation of a residential district. According to [DIN EN 50160](https://www.beuth.de/de/norm-entwurf/din-en-50160/358780917), the corrections were calculated using two methods of the voltage sensitivity analysis theory. A comparison between the methods’ robustness, accuracy and calculation speed is presented.

### Relevance to this repository 
The **"Maximum Dependencies"** showed to be an adequate method for the correction of violated voltage ranges at the grid buses of the co-simulation in this repository. This method considers all flexibility components in the grid for the calculations. Therefore, it has a high convergence rate and less power injections are needed to find solutions for voltage corrections than simplified methods.

<!--|<img src="docs\GO_Plot_Voltages.png"  width="320" height="240">|-->

<img src="docs/GO_voltages.png"  width="440" height="350">

 **Figure 4:** Voltage correction using different methods. At limited power flexibility, only the "Maximum Dependencies" method showed full convergence.

## Electric mobility integration in energy communities: trending topics and future research directions 
[link to publication](https://ieeexplore.ieee.org/document/9745450)
<details>
<summary>Citation</summary>
S. Eckhoff, H. Wagner, O. Werth, J. Gerlach, M. H. Breitner, and B. Engel, ‘Electric mobility integration in energy communities: trending topics and future research directions’, in 5th E-Mobility Power System Integration Symposium (EMOB 2021), 2021, vol. 2021, pp. 196–204. doi: 10.1049/icp.2021.2524      .
</details>

### Overview
A holistic overview of research activities on the integration of electric vehicles in energy communities that supports researchers and practitioners with the identification of relevant topics and research gaps was provided. Seven research clusters by hierarchical clustering analysis. Relevant topics include smart charging, vehicle-to-x, and considerations of uncertainty were identified. Future research should focus on open-source models and the synthesis of the knowledge base from the extensive body of literature.

#### Relevance to this repository
A research need is identified for establishing an open-source modelling tool/ co-simulation that integrates the user-sided measures for enhancing the grid capacity for EVs on a district level. Input data should be disclosed to enable further applications.

# <a name="contribution"></a> Contributing
We welcome contributions to improve the project! To get started, please follow these steps:

1. Create your feature branch (`git checkout -b feature/featurebranch`)
2. Commit your changes (`git commit -am 'changesinfeaturebranch'`)
3. Push to the branch (`git push origin feature/featurebranch`)
4. Create a new Merge Request

# <a name="citation"></a> Citation
##### If you want to cite this repository, please use the following citation:

Wagner, H.; Peñaherrera, F.; Fayed, S.; Werth, O.; Eckhoff, S.; Engel, B. et al. (2022): Co-simulation-based analysis of the grid capacity for electric vehicles in districts: the case of “Am Ölper Berge” in Lower Saxony. In: 6th E-Mobility Power System Integration Symposium (EMOB 2022). 6th E-Mobility Power System Integration Symposium (EMOB 2022). Hybrid Conference, The Hague, Netherlands, 10 October 2022: Institution of Engineering and Technology, S. 33–41.

# <a name="authorsackn"></a> Authors & Acknowledgments
- [Henrik Wagner](mailto:henrik.wagner@tu-braunschweig.de)
- [Fernando Peñaherrera](mailto:fernandoandres.penaherreravaca@offis.de)
- [Sarah Fayed](mailto:sarah.fayed@hs-emden-leer.de)
- [Sarah Eckhoff](mailto:eckhoff@iwi.uni-hannover.de)
- [Oliver Werth](mailto:werth@iwi.uni-hannover.de)

This research was funded by the Lower Saxony Ministry of Science and Culture under grant number 11-76251-13-3/19 ZN3488 (ZLE) within the Lower Saxony “SPRUNG“ of the Volkswagen Foundation. It was supported by the Center for Digital Innovations (ZDIN).

# Read-the-docs
A simple [read-the-docs](https://zdin-zle.gitlab.io/scenarios/grid-capacity-for-electric-mobility/) site is available.

# License
[GNU General Public License v3.0](LICENSE)
