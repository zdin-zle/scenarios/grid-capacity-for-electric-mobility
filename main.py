"""
@author: Henrik Wagner @elenia/TUBS and Fernando Penaherrera @UOL/OFFIS 

This file is the starting point of the simulation. The co-simulation and resulting simulation 
scenarios are configured. For more details, see
[WAG22] Wagner, Henrik; Penaherrera Vaca, Fernando; Fayed, Sarah; Werth, Oliver; Eckhoff, Sarah; 
Engel, Bernd et al. (2022): Co-Simulation-Based Analysis of the Grid Capacity for Electric Vehicles 
in Districts: The Case of "Am Ölper Berge" in Lower Saxony. In: Energynautics GmbH (Hg.): 6th 
E-Mobility Power System Integration Symposium. 6th E-Mobility Power System Integration Symposium. 
Den Haag, 10.10.2022. https://doi.org/10.1049/icp.2022.2713 

SIMULATION SCENARIOS: The district's grid capacity for EVs is analyzed based
on four different scenarios. The scenarios are defined in src/common.py

CHARGING STRATEGIES: The four different charging strategies form sub-scenarios 
to analyze the impact of (un)controlled charging. The charging scenarios are 
also defined in src/common.py

SIMULATION PERIOD: The simulation period is set to one month in March, which
has the highest electricity demand resulting from EV charging in the 
underlying datasets to form worst-case scenarios.


"""
from src.common import Scenarios, Charging_Strategies
from src.scenarios.scenario import run_scenario


def main():
    """
    Note: Run the first simulations for Scenarios.TEST
    """

    # This is the main configuration to control the simulation parameters
    # NOTE: New data only is created for first scenario or when overwrite_data = False
    configuration = {
        "start": "2020-03-04 00:00:00",
        "end": 3600 * 24 * 31,
        "step_size": 60 * 15,
        "overwrite_data": False,
        "n_cars": 49,
        "n_cs": 49,
        "soc_lims": {"soc_min": 0.1, "soc_max": 0.9},
        "status": "run_prognosis",  # "test", "run", "run_prognosis"
        "charging_strategy": "solar_charging",
    }

    # TEST SCENARIO
    """
    The TEST scenario should be run run with the default configuration and contain all components
    (e.g. Scenarios.EV_PV_STO or Scenarios.GRID_OBSV)
    """
    # run_scenario(scenario=Scenarios.EV_PV_STO,configuration=configuration)

    # SIMULATION SCENARIOS for EIS 2022 publication
    """The following loops can be used to recreate the simulations for the publication at 
    EIS 2022 conference (see https://doi.org/10.1049/icp.2022.2713)
    """
    # Set amount of simulated EVs to determine grid capacity in "Am Ölper Berge district"
    for n_cars in [
        49,
        75,
        98,
        115,
        130,
        147,
        160,
        175,
        185,
        196,
        215,
        230,
        245,
        260,
        275,
        280,
        294,
        301,
        308,
        315,
        322,
        329,
        336,
        343,
        350,
        357,
        364,
        371,
        378,
        385,
        392,
        399,
        406,
        413,
        420,
        427,
        434,
    ]:
        configuration["n_cars"] = n_cars
        configuration["status"] = "run_prognosis"
        print(f'SIMULATED EVs set to: {configuration["n_cars"]}')
        print(f"Using load and PV prognosis for charging strategies!")

        # Loop over selected simulation scenarios
        for scenario in [Scenarios.EV, Scenarios.EV_PV, Scenarios.EV_PV_STO, Scenarios.GRID_OBSV]:
            print(f"SIMULATION SCENARIO set to: {scenario.name}")

            # Check if scenario includes PV systems for solar_charging
            if scenario is [Scenarios.EV]:
                for cs_strategy in [
                    Charging_Strategies.max_P,
                    Charging_Strategies.forecast,
                    Charging_Strategies.night_charging,
                ]:
                    # overwrite configuration with current charging strategy
                    configuration["charging_strategy"] = cs_strategy.name
                    print(f"CHARGING STRATEGY set to: {cs_strategy.name}")

                    # Run the configured scenario
                    run_scenario(scenario=scenario, configuration=configuration)

            else:
                for cs_strategy in [
                    Charging_Strategies.max_P,
                    Charging_Strategies.forecast,
                    Charging_Strategies.night_charging,
                    Charging_Strategies.solar_charging,
                ]:
                    # overwrite configuration with current charging strategy
                    configuration["charging_strategy"] = cs_strategy.name
                    print(f"CHARGING STRATEGY set to: {cs_strategy.name}")

                    # Run the configured scenario
                    run_scenario(scenario=scenario, configuration=configuration)


if __name__ == "__main__":
    main()
