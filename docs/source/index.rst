.. Analysis of the Grid Capacity for Electric Mobility in Existing Districts with a Major Need for Energy Refurbishment documentation master file, created by
   sphinx-quickstart on Tue Jan 24 18:09:57 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Analysis of the Grid Capacity for Electric Mobility in Existing Districts with a Major Need for Energy Refurbishment's documentation!
================================================================================================================================================

The objective of this scenario is the analysis of the effects of the increasing number of electric vehicles on the local low-voltage grid in existing districts.

.. image:: ../../evaluation/Overview_scenario.png
  :alt: Scenario overview

As shown in the figure, a model for mapping the user-dependent grid load caused by electric mobility is coupled with the grid calculation tool `pandapower <https://www.pandapower.org/>`_
through the co-simulation platform mosaik. The electric mobility model provides time-dependent
power values of the charging processes occurring in the district. The user-specific behaviour (charging frequency, charging time), current
penetration rates and common charging capacities, coincidence factors and the region-specific typical path lengths are considered.
Pandapower can be used to determine the loads on the local low-voltage grid in the neighbourhood. For this purpose,
a load flow calculation on an annual basis, e.g. using type days, is planned. The required household and charging load profiles are imported using mosaik's CSV reader.
The final visualisation will illustrate relevant technical key figures.
In addition, the impact of electric mobility in the district on the electricity grid will be presented in a
suitable format for interested stakeholders.



.. toctree::
   :maxdepth: 3

   autoapi/index



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
