'''
@author: Fernando Penaherrera @UOL/OFFIS
'''
import unittest
from src.common import Scenarios
from src.preprosessing import prepare_scenario_data
from os.path import isfile


class Test(unittest.TestCase):


    def test_data_preparation(self):
        for scenario in Scenarios:
            scenario_file, _ = prepare_scenario_data(scenario=scenario)
            self.assertTrue(isfile(scenario_file))
        
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_data_preparation']
    unittest.main()