"""
@author: Fernando Penaherrera @UOL/OFFIS
"""
import unittest
from src.preprocessing.load_data_processing import create_household_data
import os
from os.path import isfile
from os.path import join


class Test(unittest.TestCase):
    def test_create_loads(self):
        csv_filename1, _ = create_household_data(houses=25, year=2019)
        csv_filename2, _ = create_household_data(
            filename="AOB",
            houses=20,
            units_per_house=8,
            scaling=1.8,
            file_path=os.getcwd(),
            year=2022,
        )

        for file in [csv_filename1, csv_filename2]:
            self.assertTrue(isfile(file))


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.test_create_loads']
    # this is a test push
    unittest.main()
