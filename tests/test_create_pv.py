"""
@author: Fernando Penaherrera V.@OFFIS/UOL
"""
import unittest
from src.data_preparation.create_pv_csv import create_pv_sizing, create_pv_csv_data
from os.path import isfile
import os
import logging


class Test(unittest.TestCase):
    def test_create_pv_json(self):

        for pv_scaling in [0, 2, None, 1]:
            pv_json_dir, _ = create_pv_sizing(pv_scaling=pv_scaling)

            self.assertTrue(isfile(pv_json_dir))
            os.remove(pv_json_dir)

    def test_create_pv_csv(self):
        _, data0 = create_pv_sizing(pv_scaling=0)
        _, data1 = create_pv_sizing()
        _, data2 = create_pv_sizing(pv_scaling=1)
        pv_json_dir, data3 = create_pv_sizing(pv_scaling=5)

        for pv_data in [data0, data1, data2, data3]:
            pv_csv_filename, _ = create_pv_csv_data(pv_data)
            self.assertTrue(isfile(pv_csv_filename))
            try:
                os.remove(pv_csv_filename)
            except:
                logging.info(f"{pv_csv_filename} not deleted")

        os.remove(pv_json_dir)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.test_create_pv_json']
    unittest.main()
