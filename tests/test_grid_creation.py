"""
@author: Fernando Penaherrera @UOL/OFFIS
"""
import unittest
from src.common import Scenarios
from src.preprosessing.district_grid_model import create_solve_net


class GridTest(unittest.TestCase):
    def test_create_grid(self):
        """
        Evaluates the creation of grids for different scenarios
        """

        for scenario in Scenarios:
            solve_status = create_solve_net(scenario)
            assert solve_status == True
        pass


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
