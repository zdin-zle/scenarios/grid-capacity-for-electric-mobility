'''
@author: Fernando Penaherrera @UOL/OFFIS
'''
import unittest
from src.scenarios.scenario import run_scenario
from src.common import Scenarios


class Test(unittest.TestCase):

    def test_scenarios(self):
        configuration = {
            "start": '2020-01-01 00:00:00',
            "end": 24 * 3600 * 1,
            "step_size": 60 * 15,
            "overwrite_data": False}

        run_scenario(scenario=Scenarios.SIMPLE,
                     configuration=configuration)
        pass


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_scenarios']
    unittest.main()
