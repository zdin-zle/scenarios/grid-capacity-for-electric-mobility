"""
@author: Fernando Penaherrera V.@OFFIS/UOL
"""
import unittest
from src.preprosessing.load_data_processing import create_household_data
from src.preprosessing.create_storage_sizing import create_storage_sizing
from os.path import isfile
import os


class Test(unittest.TestCase):
    def test_storage_sizing(self):

        for i in [None, 0, 1, 3, 10]:
            load_data_filename, household_data = create_household_data(houses=49, year=2020)
            storage_filename = create_storage_sizing(household_data, sto_scaling=i)
            self.assertTrue(isfile(storage_filename))
            os.remove(load_data_filename)
            os.remove(storage_filename)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
