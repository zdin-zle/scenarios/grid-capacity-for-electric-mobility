'''
@author: Fernando Penaherrera @UOL/OFFIS
'''
import unittest
from dwd_data_downloader.dwd_downloader import meteo_data_downloader
from src.common import DATA_PROCESSED_DIR
from os.path import isfile


class MeteoTest(unittest.TestCase):

    def test_meteo_data(self):
        '''
        Evaluate the creation of meteo_data for different locations
        '''
        loc_old = {
            "name": "Oldenburg",
            "id": "OLD",
            "lat": 53.1763,
            "lon": 8.1824}

        loc_bs = {
            "name": "Braunschweig",
            "id": "BS",
            "lat": 52.29,
            "lon": 10.44}

        loc_ber = {
            "name": "Berlin",
            "id": "BER",
            "lat": 52.5644,
            "lon": 13.3088}

        locations = [loc_ber, loc_old, loc_bs]

        for loc in locations:
            meteo_data_path = meteo_data_downloader(
                location=loc,
                year=2020,
                path=DATA_PROCESSED_DIR,
                delete_temp_files=True)

            self.assertTrue(isfile(meteo_data_path))
        


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
