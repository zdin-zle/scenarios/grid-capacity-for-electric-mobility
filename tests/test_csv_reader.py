'''
@author: Fernando Penaherrera @UOL/OFFIS
'''
import random
import mosaik
import os
from mosaik.util import connect_many_to_one

SIM_CONFIG = {
    'CSV': {
        'python': 'mosaik_csv:CSV'
    },
    'Load': {
        'python': 'src.components.csv_reader.load_simulator:Sim'
    },
    'DB': {
        'python': 'mosaik_hdf5:MosaikHdf5',
        #'python': 'src.components.db.hdf5_sim:MosaikHdf5'
    },
    }

random.seed(123456)
world = mosaik.World(SIM_CONFIG)
configuration = {
            "start": '2019-01-01 00:00:00',
            "end": 24 * 3600 * 2,
            "step_size": 60 * 15,
            "overwrite_data": False}

START = configuration["start"]
END = configuration["end"]
STEP_SIZE = configuration["step_size"]
OVERWRITE_DATA = configuration["overwrite_data"]
database_file_name = os.path.join(os.getcwd(), "Test.hdf5")
csv_sim = world.start(
    "CSV", 
    sim_start=START, 
    datafile="D:\OFFIS\GitLabProjects\grid-capacity-for-electric-mobility\data\processed\Household_Loads_10min.csv")

houses_sim = world.start("Load",step_size=STEP_SIZE)
db_sim = world.start('DB', step_size=STEP_SIZE, duration=END)


csv_model = csv_sim.HouseholdLoads.create(1)
houses_model = houses_sim.LoadSim.create(49,**{"name":  "HouseTest"})
database_model = db_sim.Database(filename=database_file_name)



for i in range(49):
    world.connect(csv_model[0], houses_model[i], (f'House{i}', 'p_in'))

connect_many_to_one(world, houses_model, database_model, 'P', "Q")
world.run(until=END, print_progress=False)


if __name__ == '__main__':
    pass